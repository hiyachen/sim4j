package sim4j.common.utils;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;

public class SimDateUtils {
	
	/** 时间间隔：返回毫秒数(DateTime begin = new DateTime("2023-04-19T15:24:22"); ) */
	public static long timeIntervalMS(DateTime begin, DateTime end) {
		Duration d = new Duration(begin, end);  
		long time = d.getMillis();  
//		System.out.println("时间计算(毫秒数)："+ time);
//		System.out.println("时间计算(秒数)："+ time/1000.0);
//		System.out.println("时间计算："+ time/1000.0/3600.0 +"小时前已经出发！");
		return time;
	}
	
	/** 时间间隔：返回小时数(DateTime begin = new DateTime("2023-04-19T15:24:22"); ) */
	public static float timeIntervalHours(DateTime begin, DateTime end) {
		Duration d = new Duration(begin, end);  
		long time = d.getMillis();  
//		System.out.println("时间计算(毫秒数)："+ time);
//		System.out.println("时间计算(秒数)："+ time/1000.0);
//		System.out.println("时间计算："+ time/1000.0/3600.0 +"小时前已经出发！");
		return (float) (time/1000.0/3600.0);
	} 
	
	/** 时间间隔：返回小时数(DateTime begin = new DateTime("2023-04-19T15:24:22"); )*/
	public static float toNowHours(DateTime begin) {
		return timeIntervalHours(begin, new DateTime());
	} 

//	public static void main(String[] args) {
//		DateTime begin = new DateTime("2023-04-19T15:24:22");
//		System.out.println("时间计算："+ toNowHours(begin) +"小时前已经出发！");
//		
//		
////		DateTime begin = new DateTime("2023-04-19T15:24:22");  
////		DateTime end = new DateTime();   // 现在时间
//////				DateTime end = new DateTime("2012-05-01");  
////		  
////		//计算区间毫秒数  
////		Duration d = new Duration(begin, end);  
////		long time = d.getMillis();  
////		System.out.println("时间计算(毫秒数)："+ time);
////		System.out.println("时间计算(秒数)："+ time/1000.0);
////		System.out.println("时间计算："+ time/1000.0/3600.0 +"小时前已经出发！");
////		  
////		//计算区间天数  
////		Period p = new Period(begin, end, PeriodType.days());  
////		int days = p.getDays();  
////		  
////		//计算特定日期是否在该区间内  
////		Interval i = new Interval(begin, end);  
////		boolean contained = i.contains(new DateTime("2012-03-01"));  
//
//	}

}
