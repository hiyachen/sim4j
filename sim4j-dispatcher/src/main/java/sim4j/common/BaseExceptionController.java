package sim4j.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 使用@Controller返回到要跳转页面
 * @author chenhaifeng
 * @since v1.0
 *
 */
@Controller
public class BaseExceptionController {

	private static final Logger log = LoggerFactory.getLogger(BaseExceptionController.class);
	
	@RequestMapping("/exceptionMethod")
	public String exceptionMethod(Model model) throws Exception{
		model.addAttribute("msg","没有抛出异常");
		int num = 1/10;
		log.info(String.valueOf(num));
		return "index";
	}

}
