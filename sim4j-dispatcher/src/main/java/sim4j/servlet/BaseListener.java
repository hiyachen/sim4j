package sim4j.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

//@WebListener
//public class BaseListener implements ServletContextListener {
//    @Override
//    public void contextInitialized(ServletContextEvent sce) {
//        //在这里做数据初始化操作
//    	System.out.println("---------BaseListener.java-contextInitialized()-Web项目启动了...");
//    }
//    @Override
//    public void contextDestroyed(ServletContextEvent sce) {
//        //在这里做数据备份操作
//    	System.out.println("---------BaseListener.java-contextDestroyed()-Web项目销毁了...");
//    }
//}


@WebListener
public class BaseListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        System.out.println("MyListener>>> requestDestroyed");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println("MyListener>> > requestInitialized");
    }
}

