package sim4j.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;


/**
 * http://localhost:8081/servlet?name=aaaa
 * */
@WebServlet(name="baseServlet",urlPatterns="/baseServlet")
//@WebServlet("/servlet")
public class BaseServlet extends HttpServlet {

	private static final long serialVersionUID = -250547783194836201L;

	//	public BaseServlet(){
//		System.out.println("实例化了："+this);
//	}
//	
//	@Override
//	public void init(ServletConfig config) throws ServletException {
//		super.init(config);
//		
//		System.out.println("-----BaseServlet.java--servlet初始化...");
//	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.getWriter().print("Hello BaseServlet-----BaseServlet.java--doGet()--name:"+ req.getParameter("name"));
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("--------BaseServlet.java-doPost()-----------------");
		super.doPost(req, resp);
		resp.getWriter().write("write-----------Hello BaseServlet-BaseServlet.java-doPost()--------------------");
//		resp.getWriter().write("Hello BaseServlet");
//		resp.getWriter().print("Hello BaseServlet");
//		System.out.println("------------name:" + req.getParameter("name"));
//		req.setAttribute("msg", "用户名不存在");//设置属性
//		req.getAttribute("msg");//获取属性
//		req.getAttributeNames();//获取属性名字集合
//		//响应重定向
////		resp.sendRedirect(req.getContextPath()+"/login-fail.jsp");
//		
//		//请求转发
//		req.getRequestDispatcher("/login-fail.jsp").forward(req, resp);
//
//		System.out.println("doGet:"+this);
//
//		resp.setContentType("text/html");
//		PrintWriter out = resp.getWriter();
//		out.println("");
//		out.println("");
//		out.println("  ");
//		out.println("  ");
//		out.print("    This is ");
//		out.print(this.getClass());
//		out.println(", using the GET method");
//		out.println("  ");
//		out.println("");
//		out.flush();
//		out.close();
	}

}
