package sim4j.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@WebFilter("/*")
@WebFilter("/")
public class BaseFilter implements Filter{
	
//	private static final Logger log = LoggerFactory.getLogger(BaseFilter.class);
	
	@Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("---------------------对request进行过滤--BaseFilter.java-init");

    }

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
	   System.out.println("------------------------对request进行过滤--BaseFilter.java-doFilter");
       //下面这行代码就是放行
       filterChain.doFilter(servletRequest,servletResponse);
       System.out.println("------------------------对response进行过滤");
		
	}
	
	@Override
	public void destroy() {
		 System.out.println("对request进行过滤--BaseFilter.java-destroy");
	}
	


	
}
