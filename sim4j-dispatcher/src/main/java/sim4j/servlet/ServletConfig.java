package sim4j.servlet;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletConfig {
	// 注册Servlet组件
    @Bean
    public ServletRegistrationBean getServlet(BaseServlet baseServlet){
        ServletRegistrationBean registrationBean = 
                                     new ServletRegistrationBean(baseServlet,"/baseServlet");
        return registrationBean;
    }
    
//    @Bean //必须要加到容器中才能生效
//	public ConfigurableServletWebServerFactory configurableServletWebServerFactory() {
//	    //修改tomcat相关配置
//		TomcatServletWebServerFactory factory= new TomcatServletWebServerFactory();
//		factory.setPort(8083);
//		return factory;
//	}
//	
//    //或者使用下面这种方法，泛型根据具体的servlet容器来写。
//    @Bean
//	public WebServerFactoryCustomizer<TomcatServletWebServerFactory> webServerFactoryCustomizer() {
//		return new WebServerFactoryCustomizer<TomcatServletWebServerFactory>() {
//			@Override
//			public void customize(TomcatServletWebServerFactory factory) {
//				factory.setPort(8084);
//			}
//		};
//	}
}
