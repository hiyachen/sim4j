package sim4j.dispatcher.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import sim4j.common.utils.SimDateUtils;
import sim4j.dispatcher.data.CurPos;
import sim4j.dispatcher.data.Task;

/**
 * 使用@Controller跳转到现在位置查询页面
 * @author chenhaifeng
 * @since v1.0
 *
 */
@Controller
public class CurrentPositionController {

	private static final Logger log = LoggerFactory.getLogger(CurrentPositionController.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@GetMapping("/curPos")
	// @ResponseBody
	// @RequestMapping("/index")
	public String curPos(Model model) {// 返回json 数据
		
		/** 所有任务一览 */
		  
//		LocalDateTime localDateTime = LocalDateTime.now();
//		DateTime nowtime = new DateTime();
		Date nowdate = new Date();
		List<String> currentPositions = new ArrayList<>();
		
		List<CurPos> curPoss = new ArrayList<>();
		curPoss = getAllTaskDistance();
		for (int i=0; i<curPoss.size(); i++) {
			String currentPosition = "";
			Date startTime = curPoss.get(i).getStarttime();
			if (startTime.before(nowdate)) {
				//已经飞的公里数,默认速度为600KM/小时
				DateTime begin = new DateTime(curPoss.get(i).getStarttime());
				float flew = (float) (600.0 *  SimDateUtils.toNowHours(begin));
				
				currentPosition = "飞行员：" + curPoss.get(i).getPolit() + "，离出发地："+ flew + "公里！";
				
			} else {
				currentPosition = "飞行员：" + curPoss.get(i).getPolit() + "还未出发！";
			}
			System.out.println(currentPosition);
			currentPositions.add(currentPosition);
	
		}
		model.addAttribute("currentPositions", currentPositions);
		
		return "currentPosition.html";
	}

	/**查询所有任务*/
	// select * from task;
	//	+--------+----------+----------+--------+---------------------+---------------------+
	//	| taskid | polit    | starting | ending | starttime           | endtime              |
	//	+--------+----------+----------+--------+---------------------+---------------------+
	//	|      5 | zhangsan | bj       | gz     | 2017-03-02 15:22:22 | 2017-03-02 15:22:22 |
	//	|      6 | zhangsan | bj       | gz     | 2023-03-02 15:22:22 | 2023-03-02 17:12:22 |
	//	|      7 | zhangsan | bj       | gz     | 2017-03-02 15:22:22 | 2017-03-02 15:22:22 |
	//	+--------+----------+----------+--------+---------------------+---------------------+
	public List<Task> selAllTask() {

		String sql = "select * from task;";
		List<Task> tasks = jdbcTemplate.query(sql, new RowMapper<Task>() {
			@Override
			public Task mapRow(ResultSet rs, int i) throws SQLException {
				Task task = new Task();
				task.setTaskid(rs.getInt("taskid"));
				task.setPolit(rs.getString("polit"));
				task.setStarting(rs.getString("starting"));
				task.setEnding(rs.getString("ending"));
				task.setStarttime(rs.getTimestamp("starttime"));
				task.setEndtime(rs.getTimestamp("endtime"));
				log.info("--------------CurrentPositionController.java----");
				return task;
			}
		});
		System.out.println("查询完成：" + tasks);
		return tasks;
	}
	
	/** 根据出发地和终到地计算任务距离*/
	// select * from task;
	//	+--------+----------+----------+--------+---------------------+---------------------+
	//	| taskid | polit    | starting | ending | starttime           | endtime              |
	//	+--------+----------+----------+--------+---------------------+---------------------+
	//	|      5 | zhangsan | bj       | gz     | 2017-03-02 15:22:22 | 2017-03-02 15:22:22 |
	//	|      6 | zhangsan | bj       | gz     | 2023-03-02 15:22:22 | 2023-03-02 17:12:22 |
	//	|      7 | zhangsan | bj       | gz     | 2017-03-02 15:22:22 | 2017-03-02 15:22:22 |
	//	+--------+----------+----------+--------+---------------------+---------------------+
	
	public List<CurPos> getAllTaskDistance() {

//		mysql> SELECT task.taskid AS taskid, task.polit AS polit, task.starttime as starttime, distance.kilometers AS kilometers FROM task, distance WHERE task.`STARTING` = distance.`STARTING` AND task.ending = distance.ending;
//	+--------+----------+---------------------+------------+
//	| taskid | polit    | starttime           | kilometers |
//	+--------+----------+---------------------+------------+
//	|      6 | zhangsan | 2023-03-02 15:22:22 |   1888.840 |
//	|      8 | lisi     | 2023-03-02 15:24:22 |    798.000 |
//	|      9 | wangwu   | 2023-03-02 15:42:22 |   1364.000 |
//	+--------+----------+---------------------+------------+
		String sql = "SELECT task.taskid AS taskid, task.polit AS polit, task.starttime as starttime, distance.kilometers AS kilometers FROM task, distance WHERE task.`STARTING` = distance.`STARTING` AND task.ending = distance.ending;";
		List<CurPos> curPoss = jdbcTemplate.query(sql, new RowMapper<CurPos>() {
			@Override
			public CurPos mapRow(ResultSet rs, int i) throws SQLException {
				CurPos curPos = new CurPos();
				curPos.setTaskid(rs.getInt("taskid"));
				curPos.setPolit(rs.getString("polit"));
				curPos.setStarttime(rs.getTimestamp("starttime"));
				curPos.setKilometers(rs.getFloat("kilometers"));
				log.info("--------------CurrentPositionController.java----");
				return curPos;
			}
		});
		System.out.println("查询完成：" + curPoss);
		return curPoss;
	}
	
	
}
