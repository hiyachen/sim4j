package sim4j.dispatcher.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sim4j.dispatcher.data.Task;
import sim4j.dispatcher.data.User;

/**
 * 下达作战任务页面
 * @author chenhaifeng
 * @since v1.0
 *
 */
@Controller
//@RequestMapping("/task")
public class IssueTaskController {
	
	private static final Logger log = LoggerFactory.getLogger(InterceptorController.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@GetMapping("/toIssueTask")
//	@ResponseBody
//	@RequestMapping("/index")
	public String toIssueTask(Model model){//返回json 数据  
//		List<User> list = new ArrayList<>();
//		for(int i=0; i<5; i++) {
//			User u = new User();
//			u.setId(i);
//			u.setName("zhangsan-"+i);
//			u.setAddress("beijing-" + i);
//			list.add(u);
//			log.info("---------------------IssueTaskController.java----");
//		}
//		model.addAttribute("list",list);
		model.addAttribute("task",new Task());
		model.addAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
		return "issueTask.html"; 
	}
	
//	@RequestMapping("/issueTask")
//	@RequestMapping(value = "/issueTask}", method = RequestMethod.POST,
//            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	@PostMapping("/issueTask")
	public String issueTask(@RequestParam("polit") String polit,
            @RequestParam("starting") String starting,
            @RequestParam("ending") String ending, 
            @RequestParam("starttime") String starttime,
            @RequestParam("endtime") String endtime, 
            Model model){  
		
		double speed = 600.0;
		System.out.println("--------------------------------IssueTaskController.java--issueTask()------");
		//  insert into task values(NULL, 'zhangsan', 'bj', 'gz', '2017-03-02 15:22:22', '2017-03-02 15:22:22');
		String insSql = "INSERT INTO task VALUES (null, ?,?,?,?,?,?)";
//        Object[] objects = new Object[]{"zhangsan","bj","gz"};
	    Object[] objects = new Object[]{polit, starting, ending, starttime, endtime, speed};
        jdbcTemplate.update(insSql, objects);

        System.out.println("插入数据库表完成!");
//		String sql = "select * from user;";
//		List<User> users= jdbcTemplate.query(sql, new RowMapper<User>() {
//			@Override
//			public User mapRow(ResultSet rs, int i) throws SQLException{
//				User user = new User();
//				user.setId(rs.getInt("id"));
//				user.setName(rs.getString("name"));
//				user.setAddress(rs.getString("address"));
//				user.setPassword("********");
//				return user;
//			}
//		});
//		System.out.println("插入数据库表完成："+users);
		return "tasked.html"; 
	}
	
}
