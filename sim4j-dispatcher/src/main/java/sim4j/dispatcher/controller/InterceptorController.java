package sim4j.dispatcher.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sim4j.dispatcher.data.User;

/**
 * 使用@Controller返回到要跳转页面
 * @author chenhaifeng
 * @since v1.0
 *
 */
@Controller
//@RequestMapping("/interceptor")
public class InterceptorController {
	
	private static final Logger log = LoggerFactory.getLogger(InterceptorController.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@GetMapping("/userList")
//	@ResponseBody
//	@RequestMapping("/userList")
	public String index(Model model){//返回json 数据  
		List<User> userList = new ArrayList<>();
		for(int i=0; i<5; i++) {
			User u = new User();
			u.setId(i);
			u.setName("zhangsan-"+i);
			u.setAddress("beijing-" + i);
			userList.add(u);
			log.info("---------------------UserController.java----");
		}
		model.addAttribute("list",userList);
		
		// 
		return "userList"; 
	}
	
	@RequestMapping("/interceptor/test")
	public String test(){
		return "userList.html";
		
	}
	
	@ResponseBody
	@RequestMapping("/listJson")
	public List mySqlTest(){
		
		String sql = "select * from user;";
		List<User> users= jdbcTemplate.query(sql, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int i) throws SQLException{
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setAddress(rs.getString("address"));
				user.setPassword("********");
				return user;
			}
		});
		System.out.println("查询完成："+users);
		return users;
		
	}
}
