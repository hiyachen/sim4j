package sim4j.dispatcher.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sim4j.dispatcher.data.User;

/**
 * 使用@RestController返回Json
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/json")
public class JsonController {

	// http://localhost:7901/json/user
	@RequestMapping("/user")
	public User getUser() {
		return new User(10, "zhangsan10", "addr","1234");
	}
	
//	http://localhost:7901/json/list
	@RequestMapping("/list")
	public List<User> getUserList() {
		List<User> userList = new ArrayList<>();
		User user1 = new User(21, "lisi21", "addr","1234");
		User user2 = new User(22, "lisi22", "addr","1234");
		userList.add(user1);
		userList.add(user2);
		return userList;
	}
	
//	http://localhost:7901/json/map
	@RequestMapping("/map")
	public Map<String, Object> getMap() {
		Map<String, Object> map = new HashMap<>(3);
		User user = new User(31, "wangwu31", "addr", "****");
		map.put("author", user);
		map.put("blog", "baidu.com");
		map.put("公众号", "simulation");
		return map;
	}
	
}
