package sim4j.dispatcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan("sim4j.servlet")
public class Sim4jDispatcherApplication {

	public static void main(String[] args) {
		System.out.println("---------Sim4jDispatcherApplication.java----test servlet-----------");
        SpringApplication.run(Sim4jDispatcherApplication.class, args);
    }

}
