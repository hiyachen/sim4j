package sim4j.dispatcher.aop;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mvc配置类
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

	// 拦截器
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		// 需要拦截的路径，/**表示需要拦截所有请求
		String[] addPathPatterns = { "/interceptor/test" };
//		String[] addPathPatterns = { "/test" };
		// 不需要拦截的路径，如：登录和注册
		String[] excludePathPatterns = { "/login.html", "/registry.html", "/index.html" };

		// 向系统注册一个登录提交拦截器，检查会话，所有/userManage开头的请求都经过此拦截器
		registry.addInterceptor(new BaseInterceptor()).addPathPatterns(addPathPatterns)
				.excludePathPatterns(excludePathPatterns);

		// 将加一个登录提交拦截器，检查会话，所有/userManage开头的请求都经过此拦截器
		// registry.addInterceptor(new
		// LoginInterceptor()).addPathPatterns("/userManage/**");

	}

	// 跨域访问 配置
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		// 仅仅允许来自 domain2.com 的跨域访问，并且限定访问路径为／api 、方法是 POST 或者 GET 。
		registry.addMapping("/api/**").allowedOrigins(" http : //domain2.com").allowedMethods(" POST", "GET");
	}

	// 格式化
	@Override
	public void addFormatters(FormatterRegistry registry) {
		// 注册一个格式转换器
		registry.addFormatter(new DateFormatter("yyyy-MM-dd"));
	}

	// URI 到视图的映射
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// 对于index.html的请求,设置返回的视图为index.htl 。
		registry.addViewController("/index.html").setViewName("/index.htl");
		// 所有以.do结尾的请求重定向到/index.html请求 。
		registry.addRedirectViewController("/**/*.do", "/index.html");
	}
}
