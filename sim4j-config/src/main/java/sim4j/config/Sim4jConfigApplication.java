package sim4j.config;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@NacosPropertySource(dataId = "sim4j-config.yaml", groupId = "DEFAULT_GROUP", autoRefreshed = true)
public class Sim4jConfigApplication {

	public static void main(String[] args) throws Exception {
		System.out.println("---------Sim4jConfigApplication.java----test servlet-----------");
		SpringApplication.run(Sim4jConfigApplication.class, args);
	}

}
