package sim4j.config.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;

import sim4j.config.data.Conf;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class ConfigController {
	
//	@NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
//	private boolean useLocalCache;
//	
//	@RequestMapping(value = "/get", method = GET)
//	@ResponseBody
//	public boolean get() {
//		return useLocalCache;
//	}
	
	@Autowired
	private Conf conf;
	
	@GetMapping("/config2")
	public String config1() {
		return conf.getName() + "的注释：" + conf.getDesc();
	}
	
//	@Value("${conf.name}")
////	@NacosValue(value = "${conf.name}",autoRefreshed = true)
//	private String name;
//	
//	@Value("${conf.desc}")
////	@NacosValue(value = "${conf.desc}",autoRefreshed = true)
//	private String desc;
//	
//	@GetMapping("/config1")
//	public String config1() {
//		return name + "的注释：" + desc;
//	}

	
}


