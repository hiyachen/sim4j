package sim4j.usermanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sim4jUsermanageApplication {

	public static void main(String[] args) {
		System.out.println("---------Sim4jUsermanageApplication.java----test server-discovery-----------");
        SpringApplication.run(Sim4jUsermanageApplication.class, args);
    }

}
