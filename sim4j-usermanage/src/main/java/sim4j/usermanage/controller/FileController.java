package sim4j.usermanage.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/file/")
public class FileController {
	//https://cloud.tencent.com/developer/article/1885032
	
	@RequestMapping("upload")
//	@PostMapping(value = "/file/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public String upload(@RequestParam("file") MultipartFile file) {
		System.out.println("----FileController.java---");
		String fileName = file.getOriginalFilename();
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
		String filePath = "d:/file/";
		fileName = filePath + UUID.randomUUID() + fileName; //文件重命名，防止重复
		File dest = new File(fileName);
		if(!dest.getParentFile().exists()) {
			dest.getParentFile().mkdirs();
		}
		
		try {
			file.transferTo(dest);
			return "上传成功";
		} catch (IllegalStateException e) {
			System.out.println("----FileController.java--IllegalStateException--------------");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("----FileController.java--IOException--------------");
			e.printStackTrace();
		}
		return "上传失败！";
	}


}
