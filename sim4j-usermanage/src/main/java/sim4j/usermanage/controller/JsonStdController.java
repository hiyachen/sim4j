package sim4j.usermanage.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sim4j.common.JsonResult;
import sim4j.usermanage.data.User;

/**
 * 使用@RestController返回Json标准格式
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/jsonstd")
public class JsonStdController {

	// http://localhost:7901/json/user
	@RequestMapping("/user")
	public JsonResult<User> getUser() {
		User user = new User(10, "zhangsan10", "addr","1234");
		return new JsonResult<>(user);
	}
	
//	http://localhost:7901/jsonstd/list
	@RequestMapping("/list")
	public JsonResult<List<User>> getUserList() {
		List<User> userList = new ArrayList<>();
		User user1 = new User(21, "lisi21", "addr","1234");
		User user2 = new User(22, "lisi22", "addr","1234");
		userList.add(user1);
		userList.add(user2);
		return new JsonResult<>(userList);
	}
	
//	http://localhost:7901/jsonstd/map
	@RequestMapping("/map")
	public JsonResult<Map<String, Object>> getMap() {
		Map<String, Object> map = new HashMap<>(3);
		User user = new User(31, "wangwu31", "addr","****");
		map.put("author", user);
		map.put("blog", "baidu.com");
		map.put("公众号", "simulation");
		return new JsonResult<>(map);
	}
	
}
