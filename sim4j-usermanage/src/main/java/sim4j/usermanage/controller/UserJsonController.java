package sim4j.usermanage.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;

import sim4j.usermanage.data.User;

/**
 * 使用@Controller返回到要跳转页面
 * @author chenhaifeng
 * @since v1.0
 *
 */
//@Controller
//@RequestMapping("/interceptor")
@RestController
public class UserJsonController {
	
	private static final Logger log = LoggerFactory.getLogger(UserJsonController.class);
	
	@NacosInjected
	private NamingService nameService;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
//	/** 服务发现：生产端 */
//	@GetMapping("/register")
//	public boolean getServiceName() throws NacosException {//返回json 数据  
//		nameService.registerInstance("userManage", "127.0.0.1", 8091);
//		return true;
//	}
	
//    @RequestMapping(value = "/get", method = RequestMethod.GET)
//    @ResponseBody
//    public List<Instance> get(@RequestParam String serviceName) throws NacosException {
//        return nameService.getAllInstances(serviceName);
//    }
	
	/** 服务发现：消费端 */
	@GetMapping(value = "/get")
	public List<Instance> get(@RequestParam String serviceName) throws NacosException {
		return nameService.getAllInstances(serviceName);
	}

//	@GetMapping("/userList")
////	@ResponseBody
////	@RequestMapping("/userList")
//	public String index(Model model){//返回json 数据  
//		List<User> userList = new ArrayList<>();
//		for(int i=0; i<5; i++) {
//			User u = new User();
//			u.setId(i);
//			u.setName("zhangsan-"+i);
//			u.setAddress("beijing-" + i);
//			userList.add(u);
//			log.info("---------------------UserController.java----");
//		}
//		model.addAttribute("list",userList);
//		
//		// 
//		return "userList"; 
//	}
//	
//	@RequestMapping("/test")
//	public String test(){
//		return "userList.html";
//		
//	}
	
	@ResponseBody
	@RequestMapping("/listJson")
	public List mySqlTest(){
		
		String sql = "select * from user;";
		List<User> users= jdbcTemplate.query(sql, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int i) throws SQLException{
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setAddress(rs.getString("address"));
				user.setPassword("********");
				return user;
			}
		});
		System.out.println("查询完成："+users);
		return users;
		
	}
}
