package sim4j.usermanage.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Distance {
	/** 距离id */
	private int distanceid;
	/** 始发From */
	private String starting;
	/** 终到To */
	private String ending;
	/** 公里数  */
	private float kilometers;
}
