package sim4j.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;


@ControllerAdvice
public class GlobalException {

	private static final Logger log = LoggerFactory.getLogger(GlobalException.class);
	
	@RequestMapping("/globalException")
	@ExceptionHandler(Exception.class)
	public String handleUnexpectedServer(Model model, Exception e) {
		model.addAttribute("msg","系统发生异常，请联系管理员！");
		log.info(e.getMessage());
		return "error";
	} 
}
