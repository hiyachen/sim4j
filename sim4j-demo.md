20230421演示操作顺序

一、演示分发及拦截
1：启动eclipse：
2：演示分发及拦截：启动sim4j-dispatcher
http://localhost:8081/baseServlet?name=aaa
http://localhost:8081/interceptor/test

-----你已进入拦截器-----------
2023-04-21 13:04:34.070  INFO 9580 --- [nio-8081-exec-1] sim4j.dispatcher.aop.BaseInterceptor     : ----- 拦截到了方法：test，在该方法执行之前执行----
2023-04-21 13:04:34.072  INFO 9580 --- [nio-8081-exec-1] sim4j.dispatcher.aop.BaseInterceptor     : 未登录或登录错误，请登录

二、演示服务注册、服务发现（监控）、服务消费：
1：启动Nacos，启动eclipse
D:\workspace4j\nacos-server-2.1.1\nacos\bin>startup.cmd -m standalone
http://localhost:8848/nacos/index.html

2：删除以前的命名空间，新创建命名空间spring-boot0421，记录ID：namespace: 00073623-12ad-4a8b-8361-8b0f31562b6a
3：将namespaceID写进sim4j-config的application.xml
4:依次启动sim4j-config、sim4j-usermanage、sim4j-flightmission
5：飞行任务：
任务下达：http://localhost:8092/toIssueTask
任务执行状态：http://localhost:8092/curPos

6：服务注册：
http://localhost:8091/register

7：服务发现：
http://localhost:8848/nacos/index.html#/serviceManagement?dataId=&group=&appName=&namespace=&namespaceShowName=public&pageSize=&pageNo=
可以看到userManage服务存在

8：服务停用：
关闭sim4j-usermanage工程（服务）
http://localhost:8848/nacos/index.html#/serviceManagement?dataId=&group=&appName=&namespace=&namespaceShowName=public&pageSize=&pageNo=
可以看到userManage服务没有了
   
9:服务消费：
http://localhost:8091/get?serviceName=userManage 

10：用户管理服务
json、画面
http://localhost:8091/listJson
http://localhost:8091/userList
http://localhost:8091/test


11:配置修改
配置管理->配置列表->新增
Data ID: sim4j-config.yaml （服务名.yaml）
Group : DEFAULT_GROUP
配置格式: YAML
配置内容: conf:
		    name: zhangsan
            desc: 配置修改
http://localhost:8082/config2
zhangsan的注释：配置修改
修改配置内容:
conf:
    name: wangwang
    desc: 配置修改2
http://localhost:8082/config2
wangwang的注释：配置修改2