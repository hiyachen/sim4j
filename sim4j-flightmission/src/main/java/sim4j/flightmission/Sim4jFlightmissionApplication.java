package sim4j.flightmission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

//@ServletComponentScan
@SpringBootApplication
public class Sim4jFlightmissionApplication {

	public static void main(String[] args) {
		System.out.println("---------Sim4jFlightmissionApplication.java----test servlet-----------");
        SpringApplication.run(Sim4jFlightmissionApplication.class, args);
    }

}
