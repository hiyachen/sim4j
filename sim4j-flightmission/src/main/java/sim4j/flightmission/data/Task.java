package sim4j.flightmission.data;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
	/** 任务号*/
	private int taskid; 
	/** 飞行员*/
	private String polit; 
	/** 出发地*/
	private String starting;
	/** 终到地*/
	private String ending;
	/** 出发时间*/
	private Date starttime;
	/** 终到时间*/
	private Date endtime;
	/** 速度*/
	private float speed;

}
