package sim4j.flightmission.data;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurPos {
	/** 任务号 */
	private int taskid; 
	/** 飞行员 */
	private String polit; 
	/** 出发时间 */
	private Date starttime;
	/** 公里数  */
	private float kilometers;

}
