package sim4j.flightmission.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import sim4j.common.GlobalException;
import sim4j.flightmission.data.User;

public class LoginInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HandlerMethod handlerMethod = (HandlerMethod)handler;
		Method method = handlerMethod.getMethod();
		String methodName = method.getName();
		logger.info("----- 拦截到了方法：{}，在该方法执行之前执行----", methodName);
		
//		判断用户有没有登录成功，通常登录成功会有一个token
		String token = request.getParameter("token");
		if(null == token || "".equals(token)) {
			logger.info("未登录或登录错误，请登录");
			return false;
		}
		
		return true;
//		// 获取session对象
//		HttpSession session = request.getSession();
//		// 获取存到session中的值,session有值就放行，否则不放行
//		User user = (User) session.getAttribute(UserInfoEnum.USER_NAME.getUserSessionInfo());
//		if (user == null) {
//			// 重定向
//			response.sendRedirect("/api/login");
//			return false;
//		}
//		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		logger.info("-----Controller方法调用之后执行，此时没有进行视图渲染。");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		logger.info("-----整个请求已经处理完，DispatcherServlet也渲染了对应的视图，此时可以做一些清理工作。");
	}

}
